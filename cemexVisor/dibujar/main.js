var canvas  = document.getElementById("canvas");
var context = canvas.getContext('2d');

var flag = false;

canvas.height  = window.innerHeight;
canvas.width = window.innerWidth;



var radius = 2;
context.lineWidth = radius*2;
var dragging = false;

var putPoint = function (e) {
    if(dragging)
    {

        if(flag)
        {
           context.lineTo(e.offsetX, e.offsetY);
        context.stroke();
        context.beginPath();
        context.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
        // context.arc(e.clientX, e.clientX, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(e.offsetX, e.offsetY);
        }

       
    }

}

var engage = function (e) {
    dragging  =true;
    putPoint(e);
}

var disengage = function () {
    dragging  =false;
    context.beginPath();
}


canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mouseup', disengage);



var cleanButton = document.getElementById('clean');
cleanButton.addEventListener('click', CleanCanvas);
function CleanCanvas() 
{
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
}






var lapiz = document.getElementById('Lapiz');

lapiz.addEventListener('click', disableCanvas);

function disableCanvas() 
{

    
    if (lapiz.classList.contains('active')) 
    {
        lapiz.classList.remove("active");
        flag = false;

    }else{

        flag = true;
        lapiz.className += ' active';
    }


}



