/**
 * Created by dennis-pc on 16/05/2018.
 */

$(document).ready(function() {

    var timbre = new Audio('../sonidos/Pop.wav');

    $('.timbre').on('click', function(){
        timbre.play();
    });


    $(document).on('click', '#signoutoption', function(){

     var gui = require('nw.gui');
     var win = gui.Window.get();
     win.close();
     // alert('salir');
     });


    $(document).on('click', '#backOption', function(){
        window.history.back();
    });

    $(document).on('click', '#backHome', function(){
        window.location = "../index.html";
    });

    function animateCloud() {
        $('#box').animate({ left: '+=500' }, { duration: 1800, easing: "linear" })
            .animate({ left: '-=500' }, { duration: 3500, easing: "linear", complete: animateCloud });
    }

    function animateCloud2() {
        $('#box_2').animate({ left: '-=500' }, { duration: 2500, easing: "linear" })
            .animate({ left: '+=500' }, { duration: 1100, easing: "linear", complete: animateCloud2 });
    }

    animateCloud();
    animateCloud2();

});//document.ready